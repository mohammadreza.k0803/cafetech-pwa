import React from "react";

const Img = (props) => {
  const { url } = props;
  return (
    <img
      className="profile-img"
      src="https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_1280.png"
    />
  );
};

export default Img;
