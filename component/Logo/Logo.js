import React from "react";
import Image from "next/image";

//import Logo
import logo from "./../../public/img/Logo.png";
const Logo = () => {
  return <Image src={logo} />;
};

export default Logo;
