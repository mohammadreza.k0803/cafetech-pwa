import React from "react";
//import component
import ItemList from "./ItemList";


const List = () => {
  return (
    <>
      <ItemList title="نوع استارتاپ" content="فناوری اطلاعات " />
      <ItemList title="سرمایه گذاران" content="فناپ،همراه اول" />
      <ItemList title="تاریخ شروع " content="1400/04/01 " />
      <ItemList title="محل" content="اصفهان" />
      <ItemList title="مرحله" content="راه اندازی شده" />
      <ItemList title="وضعیت" content="تایید شده" />
      <ItemList title="روز باقی مانده" content="320" />
      <ItemList title="محبوبیت" content="320 لایک" />
      <ItemList title="وب سایت" content="www.mrk.com" />
    </>
  );
};

export default List;
