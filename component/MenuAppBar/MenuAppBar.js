import React from "react";
//import material ui
import { Container } from "@material-ui/core";
//import logo
import Logo from "./../../public/img/Logo.png";
//import next
import Image from "next/image";
export default function MenuAppBar(props) {
  const { iconleft, iconright } = props;
  return (
    <div className="menuappbar">
      <Container maxWidth="lg">
        <div className="menuappbar-main">
          <div className="menuappBar-item">{iconleft}</div>
          <div className="menuappBar-item">
            <Image height={30} width={120} src={Logo} />
            {/* <p className="font-logo">CAFE FANAFARI</p> */}
          </div>
          <div className="menuappBar-item">{iconright}</div>
        </div>
      </Container>
    </div>
  );
}
