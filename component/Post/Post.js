import React, { useEffect, useState } from "react";
// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";
//import component
import Account from "../Account/Account";
//import icon
import ShareIcon from "@material-ui/icons/Share";
import SaveAltIcon from "@material-ui/icons/SaveAlt";
import BookmarkBorderIcon from "@material-ui/icons/BookmarkBorder";
import SendRoundedIcon from "@material-ui/icons/SendRounded";
import ChatBubbleOutlineRoundedIcon from "@material-ui/icons/ChatBubbleOutlineRounded";
import FavoriteBorderRoundedIcon from "@material-ui/icons/FavoriteBorderRounded";
// Import Swiper styles
import "swiper/swiper.min.css";
import "swiper/components/pagination/pagination.min.css";
// import Swiper core and required modules
import SwiperCore, { Pagination } from "swiper/core";
// install Swiper modules
SwiperCore.use([Pagination]);
//import Skeleton
import Skeleton, { SkeletonTheme } from "react-loading-skeleton";

const Post = () => {
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    setTimeout(() => {
      setLoading(false);
    }, 1500);
  }, []);

  return (
    <>
      {loading ? (
        <Skeleton height={700} style={{marginTop:"8px"}}/>
      ) : (
        <div className="card">
          <div className="top-card">
            <div className="tc-right">
              <Account />
            </div>
            <div className="tc-left">
              <ShareIcon />
            </div>
          </div>
          <div className="img-card">
            <Swiper
              spaceBetween={30}
              pagination={{
                clickable: true,
              }}
              className="mySwiper"
            >
              <SwiperSlide>
                <img src="https://www.thedigitaltransformationpeople.com/wp-content/uploads/2019/11/Innovation-is-not-about-ideas.jpg" />
              </SwiperSlide>
              <SwiperSlide>
                <img src="https://www.thedigitaltransformationpeople.com/wp-content/uploads/2019/11/Innovation-is-not-about-ideas.jpg" />
              </SwiperSlide>
              <SwiperSlide>
                <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQTt049tq-hJTYInUlKa8zmeBUgqSolj2wMBQ&usqp=CAU" />
              </SwiperSlide>
            </Swiper>
          </div>
          <div className="bottom-card">
            <div className="bc-right">
              <SaveAltIcon />
              <BookmarkBorderIcon />
            </div>
            <div className="bc-left">
              <SendRoundedIcon />
              <ChatBubbleOutlineRoundedIcon />
              <FavoriteBorderRoundedIcon />
            </div>
          </div>
          <div className="caption-card">
            <p>
              لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با
              استفاده از طراحان گرافیک است، چاپگرها و متون بلکه روزنامه و مجله
              در ستون و سطرآنچنان که لازم است، و برای شرایط فعلی تکنولوژی مورد
              نیاز، و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد،
              کتابهای زیادی در شصت و سه درصد گذشته حال و آینده، شناخت فراوان
              جامعه و متخصصان را می طلبد، تا با نرم افزارها شناخت بیشتری را برای
              طراحان رایانه ای علی الخصوص طراحان خلاقی، و فرهنگ پیشرو در زبان
              فارسی ایجاد کرد، در این صورت می توان امید داشت که تمام و دشواری
              موجود در ارائه راهکارها، و شرایط سخت تایپ به پایان رسد و زمان مورد
              نیاز شامل حروفچینی دستاوردهای اصلی، و جوابگوی سوالات پیوسته اهل
              دنیای موجود طراحی اساسا مورد استفاده قرار گیرد.
            </p>
          </div>
        </div>
      )}
    </>
  );
};

export default Post;
