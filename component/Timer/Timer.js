import React, { useEffect, useState } from "react";

const Timer = () => {
    const [state, setState] = useState({
      timer: 120,
    });
     useEffect(() => {
       const timer =
         state.timer > 0 &&
         setInterval(
           () => setState({ ...state, timer: state.timer - 1 }),
           1000
         );
       return () => clearInterval(timer);
     }, [state.timer]);
  return (
    <>
      {state.timer == 0 ? (
        <p className="jus-text">ارسال مجدد کد </p>
      ) : (
        <p className="jus-text">ارسال مجدد کد تا {state.timer} ثانیه دیگر</p>
      )}
    </>
  );
};

export default Timer;
