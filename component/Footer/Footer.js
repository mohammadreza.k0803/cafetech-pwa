import React from "react";
//import next
import Link from "next/link";
//import material ui
import { Container } from "@material-ui/core";
//import component
import ItemFooter from "./ItemFooter";
//import icon
import HomeOutlinedIcon from "@material-ui/icons/HomeOutlined";
import SearchOutlinedIcon from "@material-ui/icons/SearchOutlined";
import AddCircleOutlineOutlinedIcon from "@material-ui/icons/AddCircleOutlineOutlined";
import LocalAtmOutlinedIcon from "@material-ui/icons/LocalAtmOutlined";
import PersonOutlineOutlinedIcon from "@material-ui/icons/PersonOutlineOutlined";
const Footer = () => {
  return (
    <div className="footer">
      <Container maxWidth="lg">
        <div className="footer-main">
          <ItemFooter icon={<HomeOutlinedIcon />} path="/" name="خانه" />
          <ItemFooter
            icon={<LocalAtmOutlinedIcon />}
            path="/funding"
            name="فاندینگ"
          />

          <ItemFooter
            icon={<AddCircleOutlineOutlinedIcon />}
            path="/add"
            name="افزودن"
          />
          <ItemFooter
            icon={<SearchOutlinedIcon />}
            path="/search"
            name="جستوجو"
          />
          <ItemFooter
            icon={<PersonOutlineOutlinedIcon />}
            path="/profile"
            name="پروفایل"
          />
        </div>
      </Container>
    </div>
  );
};

export default Footer;
