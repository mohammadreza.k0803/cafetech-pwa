import React, { useEffect, useState } from "react";
//import Badge
import Badge from "./../../Badge/Badge";
// import Skeleton
import Skeleton from "react-loading-skeleton";

const Avatar = (props) => {
  const [loading, setLoading] = useState(true);
  const { name, story, person } = props;
  useEffect(() => {
    setTimeout(() => {
      setLoading(false);
    }, 1500);
  }, []);
  return (
    <>
      {loading ? (
        <div  className="avatar-skeleton">
          <Skeleton circle={true} height={60} width={60} />
          <Skeleton count={1} width={44} height={10} />
        </div>
      ) : (
        <>
          {person == true ? (
            <>
              <div className="avatar" style={{ position: "sticky" }}>
                <Badge content="+" />

                <img
                  style={story == 1 ? { borderColor: "#1976D2" } : null}
                  src="https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_1280.png"
                />
                <p>{name}</p>
              </div>
            </>
          ) : (
            <div className="avatar">
              <img
                style={story == 1 ? { borderColor: "#1976D2" } : null}
                src="https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_1280.png"
              />
              <p>{name}</p>
            </div>
          )}
        </>
      )}
    </>
  );
};

export default Avatar;
