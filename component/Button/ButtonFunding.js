import React from "react";
//import material ui
import { Button } from "@material-ui/core";
const ButtonFunding = () => {
  return (
    <div className="buttongroup-funding">
      <Button variant="contained" color="primary" className="button-funding">
        سرمایه گذاری
      </Button>
      <Button variant="contained" color="primary" className="button-funding">
        شکایت از پروژه
      </Button>
    </div>
  );
};

export default ButtonFunding;
