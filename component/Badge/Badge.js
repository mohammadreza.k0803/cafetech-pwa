import React from "react";

const Badge = (props) => {
  const { content } = props;
  return <span className="badge">{content}</span>;
};

export default Badge;
