import React, { useState } from "react";
//import component
import Logo from "./../component/Logo/Logo";
import { Button, Container, Divider } from "@material-ui/core";
import VerificationInput from "react-verification-input";
import Timer from "../component/Timer/Timer";
//import redux
import { useSelector } from "react-redux";
//import Api
import { checkcode } from "../Api";

const Checkcode = () => {
  const nummob = useSelector((state) => state.mobile);
  const [state, setState] = useState({
    activeCode: "",
    mobile: nummob,
  });

  const handlechange = (e) => {
    setState({ ...state, activeCode: e });
  };
  const handlesubmit = () => {
    checkcode(state)
      .then((res) => {
        router.push("/");
        localStorage.setItem("token", true);
        localStorage.setItem("user", true);
      })
      .catch((err) => console.log(err));
  };
  return (
    <Container maxWidth="lg">
      <div className="body-flex">
        <div className="top-div">
          <Logo />
        </div>
        <div className="mid-div1 ">
          <h3>احراز هویت</h3>
          <p className="jus-text">
            یک کد به شماره همراه شما ارسال شد، لطفا کد دریافتی خود را در کادر
            زیر وارد کنید و روی دکمه تایید کلیک کنید
          </p>
        </div>
        <div className="mid-div2">
          <div>
            <VerificationInput
              placeholder=""
              removeDefaultStyles
              length={4}
              classNames={{
                container: "container",
                character: "character",
                characterSelected: "character--selected",
              }}
              onChange={handlechange}
            />
          </div>
          <div>
            <Button
              variant="contained"
              color="primary"
              className="field"
              onClick={handlesubmit}
            >
              بررسی کد
            </Button>
          </div>
        </div>
        <div className="bottom-div">
          <Divider className="width-divider" />
          <Timer />
        </div>
      </div>
    </Container>
  );
};

export default Checkcode;
