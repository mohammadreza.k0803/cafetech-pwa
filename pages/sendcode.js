import React, { useState } from "react";
//import component
import Logo from "../component/Logo/Logo";
import {
  Button,
  Container,
  Divider,
  CircularProgress,
} from "@material-ui/core";
import StayCurrentPortraitIcon from "@material-ui/icons/StayCurrentPortrait";
//import api
import { sendcode } from "../Api";
//import swal
import Swal from "sweetalert2";
//import router
import { useRouter } from "next/router";
//import redux
import { useDispatch } from "react-redux";
import { sendcodemobile } from "../Redux/Login/action";
//import protectpage
import ProtectedRoute from "../HOC/ProtectedRoute";

const Sendcode = () => {
  const router = useRouter();
  const dispatch = useDispatch();
  const [mobile, setMobile] = useState("");
  const [loading, setLoading] = useState(false);
  const handlechange = (e) => {
    setMobile(e.target.value);
  };

  const handlesubmit = () => {
    if (mobile.length <= 10) {
      setMobile("");

      return Swal.fire({
        position: "center",
        icon: "warning",
        title: "شماره را به درستی وارد کنید",
        showConfirmButton: false,
        timer: 2000,
      });
    } else {
      setLoading(true);
      sendcode(mobile)
        .then((res) => {
          setLoading(false);
          res.data == true ? router.push("/checkcode") : null;
          dispatch(sendcodemobile(mobile));
        })
        .catch((err) => {
          setLoading(false);
          Swal.fire({
            position: "center",
            icon: "error",
            title: "خطا",
            showConfirmButton: false,
            timer: 2000,
          });
        });
    }
  };
  return (
    <Container maxWidth="lg">
      <div className="body-flex">
        <div className="top-div">
          <Logo />
        </div>
        <div className="mid-div1 ">
          <h3>خوش آمدید</h3>
          <p className="jus-text">
            برای استفاده از خدمات اپلیکیشن کافه فناوری لازم است ابتدا به حساب
            کاربری خود وارد شوید
          </p>
        </div>
        <div className="mid-div2">
          <div className="input">
            <div>
              <div className="icon-input">
                <StayCurrentPortraitIcon />
              </div>
            </div>
            <Divider
              orientation="vertical"
              style={{ width: "1px", backgroundColor: "white" }}
            />
            <div>
              <input
                type="tel"
                required
                className="input-hide"
                placeholder="09xx xxx xxxx"
                onChange={handlechange}
                value={mobile}
              />
            </div>
          </div>
          <div>
            <Button
              variant="contained"
              color="primary"
              className="field"
              onClick={handlesubmit}
            >
              {loading ? (
                <CircularProgress color="secondary" size={30} />
              ) : (
                "ورود به کافه فناوری"
              )}
            </Button>
          </div>
        </div>
        <div className="bottom-div">
          <Divider className="width-divider" />
          <p className="jus-text">
            با ورود یا ثبت نام در کافه فناوری شما
            <a href="#"> شرایط و قوانین </a>استفاده از سرویس های اپلیکیشن و سایت
            کافه فناوری را می‌پذیرید
          </p>
        </div>
      </div>
    </Container>
  );
};

export default ProtectedRoute(Sendcode);
