
import React from 'react';
//import component
import Footer from "../component/Footer/Footer";
import MenuAppBar from "../component/MenuAppBar/MenuAppBar";
import BoxProfile from '../component/ProfilePage/BoxProfile';
//import css
import styles from "../styles/Home.module.css";
//import protectpage
import ProtectedRoute from "../HOC/ProtectedRoute";
const profile = () => {
    return (
      <>
        <header>
          <MenuAppBar />
        </header>
        <main className={styles.main}>
          
        </main>
        <footer>
          <Footer />
        </footer>
      </>
    );
};

export default profile;