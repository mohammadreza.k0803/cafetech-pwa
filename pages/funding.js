import React from "react";

//import component
import Footer from "../component/Footer/Footer";
import List from "../component/List/List";
import MenuAppBar from "../component/MenuAppBar/MenuAppBar";
import SliderFunding from "../component/Slider/SliderFunding";
import ButtonFunding from "../component/Button/ButtonFunding";
import TabsFunding from "../component/TabsFunding/TabsFunding";
//import css
import styles from "../styles/Home.module.css";

const funding = () => {
  return (
    <>
      <header>
        <MenuAppBar />
      </header>
      <main className={styles.mainfounding}>
        <SliderFunding />
        <List />
        <ButtonFunding />
        <TabsFunding/>
      </main>
      <footer>
        <Footer />
      </footer>
    </>
  );
};

export default funding;
