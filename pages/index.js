import React from "react";
//import material ui
import { Container } from "@material-ui/core";
//import component
import MenuAppBar from "../component/MenuAppBar/MenuAppBar";
import Story from "../component/Story/Story";
import Post from "../component/Post/Post";
import Footer from "../component/Footer/Footer";
//import css
import styles from "../styles/Home.module.css";
//import icon
import CameraAltOutlinedIcon from "@material-ui/icons/CameraAltOutlined";
import MessageOutlinedIcon from "@material-ui/icons/MessageOutlined";
const Main = () => {
  return (
    <>
      <header>
        <MenuAppBar
          iconleft={<CameraAltOutlinedIcon />}
          iconright={<MessageOutlinedIcon />}
        />
      </header>
      <main className={styles.main}>
        <Story />
        <Container maxWidth="lg">
          <Post />
          <Post />
          <Post />
        </Container>
      </main>
      <footer>
        <Footer />
      </footer>
    </>
  );
};

export default Main;
