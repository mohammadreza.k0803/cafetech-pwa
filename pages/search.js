import React from 'react';
//import component
import Footer from "../component/Footer/Footer";
import MenuAppBar from "../component/MenuAppBar/MenuAppBar";
//import protectpage
import ProtectedRoute from "../HOC/ProtectedRoute";
const search = () => {
    return (
      <>
        <header>
          <MenuAppBar />
        </header>
        <footer>
          <Footer />
        </footer>
      </>
    );
};

export default search;