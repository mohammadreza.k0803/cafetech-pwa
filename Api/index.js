import axios from "axios";

axios.defaults.baseURL = "http://apicafetech.nicode.org/";

axios.defaults.timeout = 10000;

export const sendcode = (post) => {
  return axios.post(`/api/signin/${post}`);
};

export const checkcode = (code) => {
  return axios.post(`/api/verfiyCode/signin`, code);
};
