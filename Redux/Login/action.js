import { SENDCODE_MOBILE, USER_REDIRECT } from "./type";

export const sendcodemobile = (mobile) => {
  return {
    type: SENDCODE_MOBILE,
    payload: mobile,
  };
};

export const redirect = (user) => {
  return {
    type: USER_REDIRECT,
    payload: user,
  };
};
