import { SENDCODE_MOBILE,USER_REDIRECT } from "./type";
import { sendcodemobile } from "./action";

const initialstate = {
  mobile: "",
  user: null,
};

export const reducer = (state = initialstate, action) => {
  switch (action.type) {
    case SENDCODE_MOBILE:
      return {
        ...state,
        mobile: action.payload,
      };
    case USER_REDIRECT:
      return {
        ...state,
        user: action.payload,
      };
    default:
      return state;
  }
};
