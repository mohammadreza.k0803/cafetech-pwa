import { createStore } from "redux";

import { reducer } from "./Login/reducer";

const store = createStore(reducer);

export default store;
